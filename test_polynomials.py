from unittest import TestCase

from polynomial import Polynomial

class PolynomialTestSuite(TestCase):
    # Rachael is a math student at top-notch Arlington Tech taking Pre-calculus.
    # She is studying polynomials of a single indeterminant.  Her friend Marcos
    # tells her there is a really cool Python module that can help her with her study.
    # She imports the module and, since her favorite number is 2, she makes
    # the following Polynomial:
    # >>> from polynomial import Polynomial
    # >>> p1 = Polynomial('2x^2 + 2x + 2')
    def test_create_polynomial(self):
        self.p1 = Polynomial('2x^2 + 2x + 2')
        self.assertEqual(str(self.p1), '2x^2 + 2x + 2')

    # Then she makes another:
    # >>> p2 = Polynomial('7x^3+4x^2+x')
    def test_create_polynomial_without_spaces_between_terms(self):
        self.p2 = Polynomial('7x^3+4x^2+x')
        self.assertEqual(str(self.p2), '7x^3 + 4x^2 + x')
    #
    # She wants to add them, so she enters:
    # >>> p1 + p2
    # and sees:
    # '7x^3 + 6x^2 + 3x + 2'

