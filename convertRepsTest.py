import re

def convert(poly):
    """
    >>> convert([(3, 4), (-7, 2), (11, 0)])
    (3, 0, -7, 0, 11)
    >>> convert((3, 0, -7, 0, 11))
    [(3, 4), (-7, 2), (11, 0)]
    >>> convert([(0, 9), (+5, 3), (-9, 2)])
    (5, -9, 0, 0)
    >>> convert((5, -9, 0, 0))
    [(5, 3), (-9, 2)]
    """
    #check what it is converting from
    if type(poly) == list:
        new = []
        new.append(poly[0][0]) # append the first coefficient
        counter = 0
        for i in range(len(poly)-1): # -1 because the first one has already been added
            counter += 1 # moved counter up here so it can be reused later
            if poly[counter][1] == poly[counter-1][1]-1: # check if subsequent exponent
                new.append(poly[counter][0]) # append the coefficient
            else:
                for i in range((poly[counter-1][1]-poly[counter][1])-1):
                    new.append(0) # append correct number of zeros
                new.append(poly[counter][0]) # append coefficient
        for i in range(poly[counter][1]): # last term not constant, append correct number of zeros
            new.append(0)
        for coef in new: # gets rid of unnecessary zeros in beginning
            if coef == 0:
                new = new[coef+1:]
            else:
                break
        return tuple(new)
    elif type(poly) == tuple:
        new = []
        exp = len(poly)-1 # the index but opposite
        for num in poly: # make list of terms
            term = (num, exp)
            new.append(term)
            exp -= 1
        for term in new: # gets rid of terms with coefficient zero
            if term[0] == 0:
                new = new[:new.index(term)] + new[new.index(term)+1:]
        return new

if __name__ == '__main__':
    import doctest
    doctest.testmod()
