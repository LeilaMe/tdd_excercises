def multiply(poly1, poly2):
    """
    >>> multiply(extract_polynomial_values("3x^7-7x^2+11"), extract_polynomial_values("7x^4+4x^3-x+2"))
    ((21, 11), (12, 10), (-3, 8), (6, 7), (-49, 6), (-28, 5), (77, 4), (51, 3), (-14, 2), (-11, 1), (22, 0))
    """
    result_terms = []
    for i in poly1: # iterate through the terms of poly1
        for j in poly2: # and poly2
            # add the product of the two terms to the result set
            result_terms.append((i[0]*j[0], i[1] + j[1]))
    # sort the result by the exponent term in decreasing order
    result_terms.sort(key = lambda x: x[1], reverse=True)
    # Now combine terms of same degree
    # start by copying over the first term
    final_result = [result_terms.pop(0)] 
    for i in result_terms:  # for all terms after the first one
        if (i[1] == final_result[-1][1]): # exponent same as last one
            # so add the coefficients together
            final_result[-1] = (final_result[-1][0] + i[0], i[1])
        else: # just copy it to final_result
            final_result.append(i)

    return tuple(final_result)  
