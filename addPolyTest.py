def add(poly1, poly2):
    """
    >>> add((3, 0, -7, 0, 11), (7, 4, 0, 3))
    (3, 7, -3, 0, 14)
    >>> add((2, 0, 9), (9, 0, 0, 0, 3, -6, 2))
    (9, 0, 0, 0, 5, -6, 11)
    >>> add((5, 0, 4, 0, 4), (7, -3, +6, -8))
    (5, 7, 1, 6, -4)
    """
    poly3 = []
    count = 1
    if len(poly1) >= len(poly2):
        lngth = len(poly2)
        extra = poly1[0:-len(poly2)]
#        extra = list(extra)
    else:
        lngth = len(poly1)
        extra = poly2[0:-len(poly1)]
    for i in range(lngth):
        poly3 = poly3 + [poly1[-count] + poly2[-count]]
        count += 1
    count = 0
    for i in range(len(extra)):
        poly3.append(extra[::-1][count])
        count += 1
    poly3 = poly3[::-1]
    return tuple(poly3)

if __name__ == '__main__':
    import doctest
    doctest.testmod()
