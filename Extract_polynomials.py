import re

def extract_polynomial_values(s):
    """
    Create a list of 2-tuples of coefficents and exponents of a polynomial

      >>> extract_polynomial_values('3.0x^4+7x^2')
      [(3.0, 4.0), (7.0, 2.0)}
      >>> extract_polynomial_values('x^9-3x^4-x+42')
      [(1, 9.0), (-3.0, 4.0), (-1, 1), (42.0, 0)]
    """

    pattern = r"\s*([+-]?)(\d+)?\s*(x(\^(\d+))?)?"

# sign, coefficent, x-term, ^exponent, exponent

    matches = re.findall(pattern, s)
    result = []
    for e in matches:
        coeff = 0
        expt = 0
        if e[1] != '': # e[1] is the coefficient 
            coeff = float(e[1])
        if e[1] == '': # theres no coefficient, so implicitly its 1
            coeff = 1
        if e[2] == 'x': # theres just an x term, so exponent is 1
            expt = 1
        if (e[4] != ''): # there IS an exponent, so grab it
            expt = float(e[4])
        if e[0] == '-': # theres a minus sign, so negate the coefficent
            coeff = -coeff
        if coeff != 0 or expt != 0: # if there, add it to the result
            #print (s, e, coeff, expt)
            result.append((coeff, expt))
    result.pop()  # discard the empty match at the end
    return result

extract_polynomial_values('-3x^5+x-2')
extract_polynomial_values('-3x^5 + x - 2')

