import re

def extract_coefficients(P):
    var = re.search(r"[\D]", P)
    #if var:
        #print(var.group())
#        thing = re.compile(r"(([0-9]*\D\^[0-9]*[\+|\-]?)([0-9]*))+")
    #thing = re.compile(r"(...)+")
    #thing = re.compile(r"([0-9]+x\^?[+|-]?)")
    thing = r"([0-9]*x\^?[0-9]*[+|-]?)"
    #print(thing.findall(P))
    a = re.search(thing, P)
#    print(a.groups())
    b = re.findall(thing, P)
#    print(b[0][0])

extract_coefficients("2x^7+99")
extract_coefficients("900x^4-x+99")
extract_coefficients("999x^30-2x^7+99")

#copy and paste
import re


def get_coefficents(polynomial):
    regex = re.compile(r"(?:[+-]?\d*\.\d*|\+?(-?\d+))")
    return regex.findall(polynomial)


print(get_coefficents('2x^3+4x^2+8x-16'))

def new(P):
    thing = re.compile(r"[+-]?\d+")
    return thing.findall(P)
print(new('2x^3+4x^2+8x-16'))
print(new('2x^7+99'))
print(new(''))
print(new('900x^4-x+99'))
print(new('999x^30-2x^7+99'))

def no(P):
    """
    try to get a list of two in a tuple 
    >>> no('3x^4+7x^2')
    [(3, 4), (7, 2)]
      >>> no('x^9-3x^4-x+42')
      [(1, 9), (-3, 4), (-1, 1), (42, 0)]
    """
    pattern = r"[+-]?(\d+)\w\^(\d+)"
    listy = []
    for k in range(2):
        a = re.search(pattern, P)
        listy.append(a.groups())
    return listy
print(no('3x^4+7x^2'))

"""
>>> def M(P):
...     pat = r"[+-]?(\d+)\w\^(\d+)"
...     lit = []
...     a = re.findall(pat, P)
...     lit.append(a)
...     return lit, a
...
>>> print(M('3x^4+7x^2'))
"""

def EXTRACT(Poly):
    pattern = r"([+-]?\d+)?\w?\^?(\d+)"
    a = re.findall(pattern, Poly)
    return a
print(EXTRACT('3x^4+7x^2'))
print(EXTRACT('x^9-3x^4-x+42'))
print(EXTRACT('2x^3+4x^2+8x-16'))
print(EXTRACT('x^9-8'))


def add(one, two):
    """
    >>> add((3, 0, 11),(4, 0, 2, 0, 3))
    (4, 5, 14)
    """



