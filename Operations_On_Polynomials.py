import re

def extract_polynomial_values(s):
    """
    Create a list of 2-tuples of coefficents and exponents of a polynomial

      >>> extract_polynomial_values('3x^4+7x^2')
      [(3, 4), (7, 2)]
      >>> extract_polynomial_values('x^9-3x^4-x+42')
      [(1, 9), (-3, 4), (-1, 1), (42, 0)]
    """

    pattern = r"\s*([+-]?)(\d+)?\s*(x(\^(\d+))?)?"

# sign, coefficent, x-term, ^exponent, exponent

    matches = re.findall(pattern, s)
    result = []
    for e in matches:
        coeff = 0
        expt = 0
        if e[1] != '': # e[1] is the coefficient 
            coeff = int(e[1])
        if e[1] == '': # theres no coefficient, so implicitly its 1
            coeff = 1
        if e[2] == 'x': # theres just an x term, so exponent is 1
            expt = 1
        if (e[4] != ''): # there IS an exponent, so grab it
            expt = int(e[4])
        if e[0] == '-': # theres a minus sign, so negate the coefficent
            coeff = -coeff
        if coeff != 0 or expt != 0: # if there, add it to the result
            #print (s, e, coeff, expt)
            result.append((coeff, expt))
    result.pop()  # discard the empty match at the end
    return result

class Polynomial:
    def __init__(self, terms):
        self.terms = terms
        #print(terms)

    @classmethod
    def from_string(cls, poly_str):
        return cls(extract_polynomial_values(poly_str))

    def __str__(self):
        """
        >>> test = Polynomial("2x^2+2x+2")
        >>> print(extract_polynomial_values(test.terms))
        [(2, 2), (2, 1), (2, 0)]
        >>> T = Polynomial("x^3+2x+4")
        """
        """
        >>> T.from_string(T)
        >>> T.terms.__str__()
        """
        """
        >>> termtest = [(1, 3), (2, 2), (3, 1), (4, 0)]
        >>> terms2 =  (1, 2, 3, 4)
        >>> terms2.__str__()
        >>> termtest.__str__()
        """
        """
        >>> print(Polynomial.from_string(test))
        [(2, 2), (2, 1), (2, 0)]
        """
        """
        >>> extract_polynomial_values(test.terms).__str__()
        "2x^2 + 2x + 2"
        """
        poly_str = ""

        for term in self.terms:
            coeff, exp = term
            coeff_str = ''
            # handle negative coefficents
            if coeff < 0:
                coeff_str += ' - '
                coeff *= -1
            else:
                coeff_str += ' + '
            # handle coefficents of 1
            if coeff != 1:
                coeff_str += str(coeff)

            exp_str = ''
            # ignore exp_str for constant term, handle linear term
            if exp > 1:
                exp_str += f'x^{exp}'
            elif exp == 1:
                exp_str += 'x'

            poly_str += coeff_str + exp_str

        return poly_str[3:] if poly_str[1] == '+' else '-' + poly_str[3:]

    @classmethod
    def convert(cls, poly):
        """
        >>> Polynomial.convert((1, 2, 3, 4))
        [(1, 3), (2, 2), (3, 1), (4, 0)]
        >>> Polynomial.convert([(1, 3), (2, 2), (3, 1), (4, 0)])
        (1, 2, 3, 4)
        """
        #check what it is converting from
        if type(poly) == list:
            new = []
            new.append(poly[0][0]) # append the first coefficient
            counter = 0
            for i in range(len(poly)-1): # -1 because the first one has already been added
                counter += 1 # moved counter up here so it can be reused later
                if poly[counter][1] == poly[counter-1][1]-1: # check if subsequent exponent
                    new.append(poly[counter][0]) # append the coefficient
                else:
                    for i in range((poly[counter-1][1]-poly[counter][1])-1):
                        new.append(0) # append correct number of zeros
                    new.append(poly[counter][0]) # append coefficient
            for i in range(poly[counter][1]): # last term not constant, append correct number of zeros
                new.append(0)
            for coef in new: # gets rid of unnecessary zeros in beginning
                if coef == 0:
                    new = new[coef+1:]
                else:
                    break
            return tuple(new)
        elif type(poly) == tuple:
            new = []
            exp = len(poly)-1 # the index but opposite
            for num in poly: # make list of terms
                term = (num, exp)
                new.append(term)
                exp -= 1
            for term in new: # gets rid of terms with coefficient zero
                if term[0] == 0:
                    new = new[:new.index(term)] + new[new.index(term)+1:]
            return new

    def add(self, poly2):
        """
        >>> P1 = Polynomial.from_string("-3x^5 + x -2")
        >>> P1.add(Polynomial.from_string("7x^3 + 4x^2 +3"))
        -3x^5 + 7x^3 + 4x^2 + x + 3
        >>> Polynomial.from_string("2x^2 + 9").add(Polynomial.from_string("9x^6 + 3x^2 -6x + 2"))
        (9, 0, 0, 0, 5, -6, 11)
        >>> P1 = Polynomial.from_string("-3x^5+x-2")
        >>> P2 = Polynomial.from_string("-3x^5+x-3")
        >>> str(P1.add(P2))
        -6x^5 +2x -5
        """
        """
        >>> Poly.add((5, 0, 4, 0, 4), (7, -3, +6, -8))
        (5, 7, 1, 6, -4)
        """
        poly1_list = self.terms
        #print(poly1_list)
        poly1_list = Polynomial.convert(self.terms)
        poly2_list = Polynomial.convert(poly2.terms)
        poly3 = []
        count = 1
        if len(poly1_list) >= len(poly2_list):
            lngth = len(poly2_list)
            extra = self.terms[0:-len(poly2_list)]
        else:
            lngth = len(poly1_list)
            extra = poly2_list[0:-len(poly1_list)]
        for i in range(lngth):
            poly3 = poly3 + [poly1_list[-count] + poly2_list[-count]]
            count += 1
        #print(poly3)
        count = 0
        for i in range(len(extra)):
            poly3.append(extra[::-1][count])
            count += 1
        poly3 = poly3[::-1]
        return tuple(poly3)
        #return Polynomial.convert(tuple(poly3))
        #return poly3.__str__()

    def multiply(self, poly2):
        """
        >>> P1 = Polynomial.from_string("3x^7-7x^2+11")
        >>> str(P1.multiply(Polynomial.from_string("7x^4+4x^3-x+2")))
        '21x^11 + 12x^10 - 3x^8 + 6x^7 - 49x^6 - 28x^5 + 77x^4 + 51x^3 - 14x^2 - 11x + 22'
        """
        result_terms = []
        for i in self.terms: # iterate through the terms of poly1
            for j in poly2.terms: # and poly2
                # add the product of the two terms to the result set
                result_terms.append((i[0]*j[0], i[1] + j[1]))
        # sort the result by the exponent term in decreasing order
        result_terms.sort(key = lambda x: x[1], reverse=True)
        # Now combine terms of same degree
        # start by copying over the first term
        final_result = [result_terms.pop(0)]
        for i in result_terms:  # for all terms after the first one
            if (i[1] == final_result[-1][1]): # exponent same as last one
                # so add the coefficients together
                final_result[-1] = (final_result[-1][0] + i[0], i[1])
            else: # just copy it to final_result
                final_result.append(i)

        return self.__class__(tuple(final_result))


if __name__ == '__main__':
    import doctest
    doctest.testmod()
