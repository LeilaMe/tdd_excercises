# Test-Driven Development Exercises

A collection of small exercises to learn test-driven development with Python. 

[RegEx Presentation](https://drive.google.com/open?id=100t8iIqNb-MLDUvSCg2FuF19CrqyPkI3)
[Operations on Polynomials](https://drive.google.com/file/d/14bNmE4L1wPT2w-o5SdimThiQ7J7W7JhM/view?usp=sharing)
