def reverse_list(lst):
    """
    Reverses order of elements in list lst.
    """
    reversed = []
    for item in lst:
        reversed = [item] + reversed
    return reversed

def reverse_string(s):
    """
    Reverses order of characters in string s.
    """
    new = ""
    for a in s:
        new = a + new
    return new

def is_english_vowel(c):
    """
    Returns True if c is an english vowel
    and False otherwise.
    """
    vowels = 'aeiouyAEIOUY'
    if c in vowels:
        return True
    else:
        return False

def count_num_vowels(s):
    """
    Returns the number of vowels in a string s.
    """
    count = 0
    for c in s:
        if is_english_vowel(c) == True:
            count += 1
        else:
            pass
    return count

def histogram(l):
    """
    Converts a list of integers into a simple string histogram.
    """
    a = ""
    for i in l:
        count = i
        while count != 0:
            a +=  '#'
            count -= 1
        if count == 0 and i != l[-1]:
            a += "\n"
    return a

def get_word_lengths(s):
    """
    Returns a list of integers representing
    the word lengths in string s.
    """
    num = []
    count = 0
    count2 = 0
    for c in s:
        count2 += 1
        if c == ' ':
            num.append(count)
            count = 0
        elif len(s) == count2:
            count += 1
            num.append(count)
            return num
        else:
            count += 1

def find_longest_word(s):
    """
    Returns the longest word in string s.
    In case there are several, return the first.
    """
    num = []
    longestword = []
    listofwords = list(s.split(" "))
    count = 0
    count2 = 0
    for c in s:
        count2 += 1
        if c == ' ':
            num.append(count)
            count = 0
        elif len(s) == count2:
            count += 1
            num.append(count)
        else:
            count += 1
    for i in num:
        longestword = sorted(num)
        a = longestword[-1]
        if i == a:
            b = num.index(i)
            return listofwords[b]

def validate_dna(s):
    """
    Return True if the DNA string only contains characters
    a, c, t, or g (lower or uppercase). False otherwise.
    """
    ok = "AaCcTtGg"
    for letter in s:
        if letter in ok:
            pass
        else:
            return False
    return True

def base_pair(c):
    """
    Return the corresponding character (lowercase)
    of the base pair. If the base is not recognized,
    return 'unknown'.
    """
    ok = {'a': 't', "A": 't', 't': 'a', "T": 'a', 'c': 'g', "C": 'g', 'g': "c", "G": 'c'}
    return ok.get(c, "unknown")
#    ok = "AaCcTtGg"
 #   if c not in ok:
  #      return "unknown"
   # if c == "a" or c == "A":
    #    return "t"
#    if c == 't' or c == 'T':
 #       return "a"
  #  if c == "c" or c == "C":
   #     return "g"
    #if c == "g" or c == 'G':
     #   return "c"

def transcribe_dna_to_rna(s):
    """
    Return string s with each letter T replaced by U.
    Result is always uppercase.
    """
   # for letter in s:
        #if letter == "T" or letter == 't':
        #   s[s.index(letter)] = "U"
         #forgot strings are immutable
    while 't' in s or "T" in s:
        pos = s.find("T")
        s = s[0:pos] + "U" + s[pos+1:]
        pos = s.find("t")
        s = s[0:pos] + "U" + s[pos+1:]
    return s.upper()

def get_complement(s):
    """
    Return the DNA complement in uppercase
    (A -> T, T-> A, C -> G, G-> C).
    """
    new = ""
    for letter in s:
        if letter == "C" or letter == 'c':
            new = new + "G"
        if letter == "G" or letter == 'g':
            new = new + "C"
        if letter == "A" or letter == 'a':
            new = new + "T"
        if letter == "T" or letter == 't':
            new = new + "A"
    return new

def get_reverse_complement(s):
    """
    Return the reverse complement of string s
    (complement reversed in order).
    """
    c = get_complement(s)
    return c[::-1]

def remove_substring(substring, string):
    """
    Returns string with all occurrences of substring removed.
    """
    if  substring in string:
        new_string = string.replace(substring, '')
    return new_string

def get_position_indices(triplet, dna):
    """
    Returns list of position indices for a specific triplet (3-mer)
    in a DNA sequence. We start counting from 0
    and jump by 3 characters from one position to the next.
    """
    new_list = []
    count = 0
    list_of_words = list(dna[i:i+3] for i in range(0, len(dna), 3))
    for i in list_of_words:
        if i == triplet:
            new_list.append(count)
        count += 1
    return new_list

def get_3mer_usage_chart(s):
    """
    This routine implements a 'sliding window'
    and extracts all possible consecutive 3-mers.
    It counts how often they appear and returns
    a list of tuples with (name, occurrence).
    The list is alphabetically sorted by the name
    of the 3-mer.
    """
    new_list = []
    tuple_counts = {}
    for i in range(0, len(s)-2, 1):
        tuple = s[i:i+3]
        if (tuple in tuple_counts):
            tuple_counts[tuple] = tuple_counts[tuple] +1
        else: 
            tuple_counts[tuple] = 1
    for k in sorted(tuple_counts):
        new_list.append((k, tuple_counts[k]))
    return new_list



